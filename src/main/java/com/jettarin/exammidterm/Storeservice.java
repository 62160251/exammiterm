/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jettarin.exammidterm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author NITRO 5
 */
public class Storeservice {

    private static ArrayList<Product> productList = new ArrayList<>();

    static {

        
        load();

    }

    public static boolean clearAll() {
        productList.clear();
        return true;
    }
    static int TotalItem() {
        int sumtotalItem = 0;

        for (int i = 0; i < productList.size(); i++) {
            sumtotalItem += productList.get(i).getAmount();
        }

        return sumtotalItem;
    }

    static double totalPrice() {

        double sumprice = 0.00;

        for (int i = 0; i < productList.size(); i++) {
            sumprice += productList.get(i).getAmount() * productList.get(i).getPrice();
        }

        return sumprice;
    }

    //create(c
    public static boolean addProduct(Product product) {
        productList.add(product);
        save();
        return true;
    }//delete(D)

    public static boolean deleteProduct(Product product) {
        productList.remove(product);
        save();
        return true;
    }

    public static boolean deleteProduct(int index) {
        productList.remove(index);
        save();
        return true;
    }//read(R)

    public static ArrayList<Product> getProduct() {
        save();
        return productList;
    }

    public static Product getProduct(int index) {
        save();
        return productList.get(index);
    }//update(U)

    public static boolean updateProduct(int index, Product user) {
        productList.set(index, user);
        save();
        return true;
    }

    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("Aon.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(productList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Storeservice.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Storeservice.class.getName())
                    .log(Level.SEVERE, null, ex);
        }

    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("Aon.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            productList = (ArrayList<Product>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Storeservice.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Storeservice.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Storeservice.class.getName())
                    .log(Level.SEVERE, null, ex);
        }

    }
}
